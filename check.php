<?php
require_once 'Products/RenderTypes.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $type = $_POST['type'];
    echo RenderTypes::{$type}();
}