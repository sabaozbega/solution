<?php
require_once 'Request.php';
require_once 'Products/Product.php';
class Router
{
    private $request;
    private $database;
    public function __construct($database)
    {
        $this->request = new Request();
        $this->database = $database;
    }

    public function get() {
        // path info doesn't exists on '/' url
        if(!$this->request->getPath()) {
            $productList = new ProductList($this->database);
            echo $productList->renderList();
        }
        else if($this->request->getPath() === '/product/create') {
            require_once 'views/create.php';
        }
    }


    public function post() {
        if($this->request->getPath() === '/product/delete' && $this->request->getMethod() === 'POST') {
            $this->database->removeProduct($_POST['data']);
        }
        elseif ($this->request->getPath() === '/product' && $this->request->getMethod() === 'POST') {
            $body = $this->request->getBody();
            $product = new Product($body);
            $this->database->insertProduct($product);
            header('Location:/');
        }
    }

}