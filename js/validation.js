// Handling with PHP OOP
let type = document.getElementById('type_switch').value;
let form = document.getElementById('form');
let btn = document.getElementById('btn');
let inputs = document.getElementsByTagName('input');
let span = document.getElementById('msg');

// form validation
btn.addEventListener('click',() => {
    for (let input of inputs) {
        if(input.value === '') {
            span.textContent = "Please fill all fields";
            return false;
        }
    }
    form.submit();
})

const ajax = (data) => {
    $.ajax({
        type: "POST",
        data: {
            type:data
        },
        url: "../check.php",
        success: function(response) {
            $('#content').html(response);
        }
    });
};

$(document).ready(function(){
    // when page loads the default type form
    ajax(type);
    // after changing the type, bring the necessary HTML
    $("#type_switch").change(function(){
        type = $(this).val();
        ajax(type);
        inputs = document.getElementsByTagName('input');
})});


// Product type differentiation handling with vanilla JS (no PHP)
/*
const select = document.getElementById('type_switch');
const bookType = document.getElementById('book');
const furnitureType = document.getElementById('furniture');
const diskType = document.getElementById('dvd');

book();

let type = select.value;
select.addEventListener('change',() => {
    type = select.value;
    window[type]();
});

function dvd(){
    diskType.classList.add('active');
    furnitureType.classList.remove('active');
    bookType.classList.remove('active');
}

function book() {
    bookType.classList.add('active');
    furnitureType.classList.remove('active');
    diskType.classList.remove('active');
}

function furniture() {
    furnitureType.classList.add('active');
    bookType.classList.remove('active');
    diskType.classList.remove('active');
}
*/

