<?php


class Request
{
public function getPath() {
    return $_SERVER['PATH_INFO'] ?? false;
}

public function getMethod() {
    return $_SERVER['REQUEST_METHOD'];
}

public function getBody() {
    if ($this->getMethod() === 'GET') {
        $body = [];
        foreach ($_GET as $key => $value){
            $body[$key] = filter_input(INPUT_GET,$key,FILTER_SANITIZE_SPECIAL_CHARS);
        }
        return $body;
    }
    if ($this->getMethod() === 'POST') {
        $body = [];
        foreach ($_POST as $key => $value) {
            $body[$key] = filter_input(INPUT_POST,$key,FILTER_SANITIZE_SPECIAL_CHARS);
        }
        return $body;
    }
}

}