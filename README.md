# Change
- Avoided conditionals
- Changed database structure (added one more column) so re-create it
# About solution
- This solution has been built assuming that apache is configured so that
- every request is redirected to index.php so run it using built in server
# URL-s
- / - product list
- /product/create - create a new product
# DB
- In order to create the database the same structure as i planned,
- use db/migration.php by entering by browser or in command line - 
- php migration.php in db directory

# Type change
- I have included both - implementation with only javascript and also
handling with OOP.