<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/productList.css">
    <link rel="stylesheet" href="css/shared.css">
    <title>Product List</title>
    <script src="js/app.js" defer></script>
</head>
<body>
    <header>
        <div class="header">
            <h1>Product List</h1>

        <form action="/product/delete" method="POST">
            <select id="massDeleteAction">
                <option value="delete">Mass Delete Action</option>
            </select>
            <input type="hidden" id="hidden" name="data">
            <button id="btn">Apply</button>
        </form>
        </div>
    </header>
    
    <main>
        <div class="wrapper">
        <?php foreach ($this->data as $data): ?>
            <div class="box">
                <form action="#">
                    <input type="checkbox" value="<?php echo $data['sku'];?>">
                </form>

                <div class="description">
                    <span><?php echo $data['sku'];?></span>
                    <span><?php echo $data['name']?></span>
                    <span><?php echo $data['price']?>$</span>
                    <?php echo HandleListDifference::{$data['type']}($data); ?>
                </div>

            </div>
            <?php endforeach;?>
        </div>
    </main>
</body>
</html>