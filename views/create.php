<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Product</title>
    <link rel="stylesheet" href="../css/shared.css">
    <link rel="stylesheet" href="../css/product_add.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="../js/validation.js" defer></script>
</head>
<body>
<header>
    <div class="header">
        <h1>Product Add</h1>
            <button id="btn">Save</button>
    </div>
</header>
<main>
    <span id="msg"></span>
    <form action="/product" method="POST" id="form">
        <div class="input">
            <label for="sku">SKU</label>
            <input type="text" name="sku" required>
        </div>
        <div class="input">
            <label for="name">Name</label>
            <input type="text" name="name" required>
        </div>
        <div class="input">
            <label for="price">Price</label>
            <input type="text" name="price" required>
        </div>
        <div class="input">
            <label for="price">Type Switcher</label>
            <select name="type" id="type_switch">
                <option value="dvd">DVD-disc</option>
                <option value="book">Book</option>
                <option value="furniture">Furniture</option>
            </select>
        </div>
    <div id="content"></div>
    </form>
</main>
</body>

</html>