<?php


class Product
{
    public $sku;
    public $name;
    public $price;
    public $size;
    public $weight;
    public $height;
    public $width;
    public $length;
    public $type;

    public function __construct(array $body)
    {
        $this->sku = $body['sku'];
        $this->name = $body['name'];
        $this->price = $body['price'];
        $this->size = $body['size'] ?? null;
        $this->weight = $body['weight'] ?? null;
        $this->height = $body['height'] ?? null;
        $this->width = $body['width'] ?? null;
        $this->length = $body['length'] ?? null;
        $this->type = $body['type'];
    }
}