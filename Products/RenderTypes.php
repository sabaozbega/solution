<?php


class RenderTypes
{
    public static function dvd() {
        return '<div class="input dvd" id="dvd">
            <label for="size">Size</label>
            <input type="text" name="size" required>
            <p><span>Choose size for DVD Disk (In MB)  </span></p>
        </div>';
    }
    public static function book() {
        return  '<div class="input book" id="book">
            <label for="weight">Weight</label>
            <input type="text" name="weight" required>
            <p><span>Point book\'s weight in KG</span></p>
        </div>';
    }
    public static function furniture() {
        return ' <div class="input furniture" id="furniture">
            <div class="height">
                <label for="height">Height</label>
                <input type="text" name="height" required>
            </div>
            <div class="width">
                <label for="width">Width</label>
                <input type="text" name="width" required>
            </div>
            <div class="length">
                <label for="length">Length</label>
                <input type="text" name="length" required>
            </div>
            <p><span>Please provide dimensions in HxWxL format</span></p>
        </div>';
    }
}