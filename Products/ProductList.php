<?php

require_once 'HandleListDifference.php';
class ProductList
{
    private $data;
    private $database;

    public function __construct($database)
    {
        $this->database = $database;
    }

    private function getData() {
       $this->data = $this->database->getFromDB();
    }

    public function renderList() {
        $this->getData();
        ob_start();
        require_once "views/list.php";
        return ob_get_clean();
    }


}