<?php


class HandleListDifference
{
public static function dvd($data) {
    return '<span>Size: '. $data['size'] .'MB</span>';
}

public static function book($data) {
    return '<span>Weight: '. $data['weight'] .'KG</span>';
}

public static function furniture($data) {
    return '<span>Dimension: '. $data['height'].'x'.$data['width'].'x'. $data['length'] .'</span>';
}

}