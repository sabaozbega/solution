<?php
/*
 * Every request is redirecting to this index.html
 */
require_once 'Products/ProductList.php';
require_once 'db/Database.php';
require_once 'Router.php';


$database = new Database();
$router = new Router($database);

$router->get(); // manages rendering necessary view for url
$router->post(); // manages post requests