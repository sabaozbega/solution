<?php
$config = require_once '../config/config.php';


$servername =$config['host'];
$username = $config['username'];
$password = $config['password'];
$database = $config['database'];


$conn = new \PDO("mysql:host=$servername",$username,$password);

try {
    $sql = "CREATE DATABASE IF NOT EXISTS $database";
    $conn->exec($sql);
    $conn->query("USE $database");
    $sql = "CREATE TABLE products (
            id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            sku VARCHAR(200) NOT NULL,
            name VARCHAR(200) NOT NULL,
            price FLOAT NOT NULL,
            size INT(6),
            weight FLOAT,
            height FLOAT,
            width FLOAT,
            length FLOAT,
            type VARCHAR(50),
            add_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            )";
    $conn->exec($sql);
}catch (PDOException $e) {
    echo $e->getMessage().'<br>';
}