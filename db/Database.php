<?php


class Database
{
    private $pdo;

    public function __construct()
    {
       $config = require_once __DIR__.'/../config/config.php';

        $servername = $config['host'];
        $username = $config['username'];
        $password = $config['password'];
        $database = $config['database'];
        $this->pdo = new \PDO("mysql:host=$servername",$username,$password);
        $this->pdo->query("USE $database");
    }

    public function getFromDB() {
        $sql = "SELECT * FROM products";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function removeProduct(string $condition) {
        try {
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            $sql = "DELETE FROM products where sku in ($condition)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
        }catch (ErrorException $e) {
            echo $e->getMessage();
        }
       header("Location:/");
    }

    public function insertProduct(Product $product) {
        $sql = "INSERT INTO products(sku,name,price,size,weight,height,width,length,type)
VALUES(:sku,:name,:price,:size,:weight,:height,:width,:length,:type)";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':sku',$product->sku);
        $stmt->bindParam(':name',$product->name);
        $stmt->bindParam(':price',$product->price);
        $stmt->bindParam(':size',$product->size);
        $stmt->bindParam(':weight',$product->weight);
        $stmt->bindParam(':height',$product->height);
        $stmt->bindParam(':width',$product->width);
        $stmt->bindParam(':length',$product->length);
        $stmt->bindParam(':type',$product->type);
        $stmt->execute();
    }
}